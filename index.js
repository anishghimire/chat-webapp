const express = require('express');
const app = express();
const mysql = require('mysql');
const path = require('path');

const http = require('http').createServer(app)
const io = require('socket.io')(http);
const port = 3000;

const connection = mysql.createConnection({
  host: "localhost",
	user: "root",
	password: "password",
	database: "chat"
});

connection.connect(function (error) {
  error ? console.log(error) : console.log('Connected to database');
  console.log(__dirname +'/index.html')
});


io.on("connection", function (socket) {
  socket.on("user_has_entered", function(user) {
    io.emit("user_has_entered", user)
  })
  console.log("User connected", socket.id);
  socket.on("new_message", function (data) {
    console.log("Client says", data);
    connection.query("INSERT INTO messages (nickname, message) VALUES ('"+ data.nickname +"', '" + data.message + "')", function(error, result){
      io.emit("new_message", data)
    });
  });
  
  socket.on("disconnect", function() {
    console.log("User disconnected")
  })
});

app.use(function (request, result, next) {
	result.setHeader("Access-Control-Allow-Origin", "*");
	next();
});

app.use('/js', express.static(path.join(__dirname, 'js')));

app.get("/get_messages", function (request, result) {
	connection.query("SELECT * FROM messages", function (error, messages) {
		result.end(JSON.stringify(messages));
	});
});

app.get('/', function(req, res) {
  res.sendFile('index.html', {root: __dirname})
});

http.listen(port, function () {
  console.log("Listening to port " + port)
});

